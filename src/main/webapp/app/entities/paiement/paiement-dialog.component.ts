import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Paiement } from './paiement.model';
import { PaiementPopupService } from './paiement-popup.service';
import { PaiementService } from './paiement.service';
import { Client, ClientService } from '../client';

@Component({
    selector: 'jhi-paiement-dialog',
    templateUrl: './paiement-dialog.component.html'
})
export class PaiementDialogComponent implements OnInit {

    paiement: Paiement;
    isSaving: boolean;

    clients: Client[];
    datepaiementDp: any;
    dateconfirmationDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private paiementService: PaiementService,
        private clientService: ClientService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.clientService.query()
            .subscribe((res: HttpResponse<Client[]>) => { this.clients = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.paiement.id !== undefined) {
            this.subscribeToSaveResponse(
                this.paiementService.update(this.paiement));
        } else {
            this.subscribeToSaveResponse(
                this.paiementService.create(this.paiement));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Paiement>>) {
        result.subscribe((res: HttpResponse<Paiement>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Paiement) {
        this.eventManager.broadcast({ name: 'paiementListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackClientById(index: number, item: Client) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-paiement-popup',
    template: ''
})
export class PaiementPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private paiementPopupService: PaiementPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.paiementPopupService
                    .open(PaiementDialogComponent as Component, params['id']);
            } else {
                this.paiementPopupService
                    .open(PaiementDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
