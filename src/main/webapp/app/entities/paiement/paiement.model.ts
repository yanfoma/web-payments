import { BaseEntity } from './../../shared';

export class Paiement implements BaseEntity {
    constructor(
        public id?: number,
        public etat?: string,
        public methode?: string,
        public montantarticle?: number,
        public fraisoperateur?: number,
        public totalfrais?: number,
        public confirmepar?: string,
        public datepaiement?: any,
        public devise?: string,
        public dateconfirmation?: any,
        public clientId?: number,
    ) {
    }
}
