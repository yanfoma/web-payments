import { BaseEntity } from './../../shared';

export class Client implements BaseEntity {
    constructor(
        public id?: number,
        public nom?: string,
        public prenom?: string,
        public mail1?: string,
        public mail2?: string,
        public telephone1?: string,
        public telephone2?: string,
        public paiements?: BaseEntity[],
    ) {
    }
}
