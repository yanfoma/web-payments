package ys.paiement.service.dto;


import java.time.LocalDate;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Paiement entity.
 */
public class PaiementDTO implements Serializable {

    private Long id;

    private String etat;

    private String methode;

    private Float montantarticle;

    private Float fraisoperateur;

    private Float totalfrais;

    private String confirmepar;

    private LocalDate datepaiement;

    private String devise;

    private LocalDate dateconfirmation;

    private Long clientId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getMethode() {
        return methode;
    }

    public void setMethode(String methode) {
        this.methode = methode;
    }

    public Float getMontantarticle() {
        return montantarticle;
    }

    public void setMontantarticle(Float montantarticle) {
        this.montantarticle = montantarticle;
    }

    public Float getFraisoperateur() {
        return fraisoperateur;
    }

    public void setFraisoperateur(Float fraisoperateur) {
        this.fraisoperateur = fraisoperateur;
    }

    public Float getTotalfrais() {
        return totalfrais;
    }

    public void setTotalfrais(Float totalfrais) {
        this.totalfrais = totalfrais;
    }

    public String getConfirmepar() {
        return confirmepar;
    }

    public void setConfirmepar(String confirmepar) {
        this.confirmepar = confirmepar;
    }

    public LocalDate getDatepaiement() {
        return datepaiement;
    }

    public void setDatepaiement(LocalDate datepaiement) {
        this.datepaiement = datepaiement;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public LocalDate getDateconfirmation() {
        return dateconfirmation;
    }

    public void setDateconfirmation(LocalDate dateconfirmation) {
        this.dateconfirmation = dateconfirmation;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PaiementDTO paiementDTO = (PaiementDTO) o;
        if(paiementDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), paiementDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PaiementDTO{" +
            "id=" + getId() +
            ", etat='" + getEtat() + "'" +
            ", methode='" + getMethode() + "'" +
            ", montantarticle=" + getMontantarticle() +
            ", fraisoperateur=" + getFraisoperateur() +
            ", totalfrais=" + getTotalfrais() +
            ", confirmepar='" + getConfirmepar() + "'" +
            ", datepaiement='" + getDatepaiement() + "'" +
            ", devise='" + getDevise() + "'" +
            ", dateconfirmation='" + getDateconfirmation() + "'" +
            "}";
    }
}
