package ys.paiement.service.mapper;

import ys.paiement.domain.*;
import ys.paiement.service.dto.PaiementDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Paiement and its DTO PaiementDTO.
 */
@Mapper(componentModel = "spring", uses = {ClientMapper.class})
public interface PaiementMapper extends EntityMapper<PaiementDTO, Paiement> {

    @Mapping(source = "client.id", target = "clientId")
    PaiementDTO toDto(Paiement paiement);

    @Mapping(source = "clientId", target = "client")
    Paiement toEntity(PaiementDTO paiementDTO);

    default Paiement fromId(Long id) {
        if (id == null) {
            return null;
        }
        Paiement paiement = new Paiement();
        paiement.setId(id);
        return paiement;
    }
}
