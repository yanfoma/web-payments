package ys.paiement.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Paiement.
 */
@Entity
@Table(name = "paiement")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Paiement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "etat")
    private String etat;

    @Column(name = "methode")
    private String methode;

    @Column(name = "montantarticle")
    private Float montantarticle;

    @Column(name = "fraisoperateur")
    private Float fraisoperateur;

    @Column(name = "totalfrais")
    private Float totalfrais;

    @Column(name = "confirmepar")
    private String confirmepar;

    @Column(name = "datepaiement")
    private LocalDate datepaiement;

    @Column(name = "devise")
    private String devise;

    @Column(name = "dateconfirmation")
    private LocalDate dateconfirmation;

    @ManyToOne
    private Client client;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEtat() {
        return etat;
    }

    public Paiement etat(String etat) {
        this.etat = etat;
        return this;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getMethode() {
        return methode;
    }

    public Paiement methode(String methode) {
        this.methode = methode;
        return this;
    }

    public void setMethode(String methode) {
        this.methode = methode;
    }

    public Float getMontantarticle() {
        return montantarticle;
    }

    public Paiement montantarticle(Float montantarticle) {
        this.montantarticle = montantarticle;
        return this;
    }

    public void setMontantarticle(Float montantarticle) {
        this.montantarticle = montantarticle;
    }

    public Float getFraisoperateur() {
        return fraisoperateur;
    }

    public Paiement fraisoperateur(Float fraisoperateur) {
        this.fraisoperateur = fraisoperateur;
        return this;
    }

    public void setFraisoperateur(Float fraisoperateur) {
        this.fraisoperateur = fraisoperateur;
    }

    public Float getTotalfrais() {
        return totalfrais;
    }

    public Paiement totalfrais(Float totalfrais) {
        this.totalfrais = totalfrais;
        return this;
    }

    public void setTotalfrais(Float totalfrais) {
        this.totalfrais = totalfrais;
    }

    public String getConfirmepar() {
        return confirmepar;
    }

    public Paiement confirmepar(String confirmepar) {
        this.confirmepar = confirmepar;
        return this;
    }

    public void setConfirmepar(String confirmepar) {
        this.confirmepar = confirmepar;
    }

    public LocalDate getDatepaiement() {
        return datepaiement;
    }

    public Paiement datepaiement(LocalDate datepaiement) {
        this.datepaiement = datepaiement;
        return this;
    }

    public void setDatepaiement(LocalDate datepaiement) {
        this.datepaiement = datepaiement;
    }

    public String getDevise() {
        return devise;
    }

    public Paiement devise(String devise) {
        this.devise = devise;
        return this;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public LocalDate getDateconfirmation() {
        return dateconfirmation;
    }

    public Paiement dateconfirmation(LocalDate dateconfirmation) {
        this.dateconfirmation = dateconfirmation;
        return this;
    }

    public void setDateconfirmation(LocalDate dateconfirmation) {
        this.dateconfirmation = dateconfirmation;
    }

    public Client getClient() {
        return client;
    }

    public Paiement client(Client client) {
        this.client = client;
        return this;
    }

    public void setClient(Client client) {
        this.client = client;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Paiement paiement = (Paiement) o;
        if (paiement.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), paiement.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Paiement{" +
            "id=" + getId() +
            ", etat='" + getEtat() + "'" +
            ", methode='" + getMethode() + "'" +
            ", montantarticle=" + getMontantarticle() +
            ", fraisoperateur=" + getFraisoperateur() +
            ", totalfrais=" + getTotalfrais() +
            ", confirmepar='" + getConfirmepar() + "'" +
            ", datepaiement='" + getDatepaiement() + "'" +
            ", devise='" + getDevise() + "'" +
            ", dateconfirmation='" + getDateconfirmation() + "'" +
            "}";
    }
}
