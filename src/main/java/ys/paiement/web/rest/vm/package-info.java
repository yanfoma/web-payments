/**
 * View Models used by Spring MVC REST controllers.
 */
package ys.paiement.web.rest.vm;
