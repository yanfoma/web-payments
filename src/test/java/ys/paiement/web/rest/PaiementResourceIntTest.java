package ys.paiement.web.rest;

import ys.paiement.YsPaiementApp;

import ys.paiement.domain.Paiement;
import ys.paiement.repository.PaiementRepository;
import ys.paiement.service.PaiementService;
import ys.paiement.service.dto.PaiementDTO;
import ys.paiement.service.mapper.PaiementMapper;
import ys.paiement.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static ys.paiement.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PaiementResource REST controller.
 *
 * @see PaiementResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = YsPaiementApp.class)
public class PaiementResourceIntTest {

    private static final String DEFAULT_ETAT = "AAAAAAAAAA";
    private static final String UPDATED_ETAT = "BBBBBBBBBB";

    private static final String DEFAULT_METHODE = "AAAAAAAAAA";
    private static final String UPDATED_METHODE = "BBBBBBBBBB";

    private static final Float DEFAULT_MONTANTARTICLE = 1F;
    private static final Float UPDATED_MONTANTARTICLE = 2F;

    private static final Float DEFAULT_FRAISOPERATEUR = 1F;
    private static final Float UPDATED_FRAISOPERATEUR = 2F;

    private static final Float DEFAULT_TOTALFRAIS = 1F;
    private static final Float UPDATED_TOTALFRAIS = 2F;

    private static final String DEFAULT_CONFIRMEPAR = "AAAAAAAAAA";
    private static final String UPDATED_CONFIRMEPAR = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATEPAIEMENT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATEPAIEMENT = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_DEVISE = "AAAAAAAAAA";
    private static final String UPDATED_DEVISE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATECONFIRMATION = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATECONFIRMATION = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private PaiementRepository paiementRepository;

    @Autowired
    private PaiementMapper paiementMapper;

    @Autowired
    private PaiementService paiementService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPaiementMockMvc;

    private Paiement paiement;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PaiementResource paiementResource = new PaiementResource(paiementService);
        this.restPaiementMockMvc = MockMvcBuilders.standaloneSetup(paiementResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Paiement createEntity(EntityManager em) {
        Paiement paiement = new Paiement()
            .etat(DEFAULT_ETAT)
            .methode(DEFAULT_METHODE)
            .montantarticle(DEFAULT_MONTANTARTICLE)
            .fraisoperateur(DEFAULT_FRAISOPERATEUR)
            .totalfrais(DEFAULT_TOTALFRAIS)
            .confirmepar(DEFAULT_CONFIRMEPAR)
            .datepaiement(DEFAULT_DATEPAIEMENT)
            .devise(DEFAULT_DEVISE)
            .dateconfirmation(DEFAULT_DATECONFIRMATION);
        return paiement;
    }

    @Before
    public void initTest() {
        paiement = createEntity(em);
    }

    @Test
    @Transactional
    public void createPaiement() throws Exception {
        int databaseSizeBeforeCreate = paiementRepository.findAll().size();

        // Create the Paiement
        PaiementDTO paiementDTO = paiementMapper.toDto(paiement);
        restPaiementMockMvc.perform(post("/api/paiements")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paiementDTO)))
            .andExpect(status().isCreated());

        // Validate the Paiement in the database
        List<Paiement> paiementList = paiementRepository.findAll();
        assertThat(paiementList).hasSize(databaseSizeBeforeCreate + 1);
        Paiement testPaiement = paiementList.get(paiementList.size() - 1);
        assertThat(testPaiement.getEtat()).isEqualTo(DEFAULT_ETAT);
        assertThat(testPaiement.getMethode()).isEqualTo(DEFAULT_METHODE);
        assertThat(testPaiement.getMontantarticle()).isEqualTo(DEFAULT_MONTANTARTICLE);
        assertThat(testPaiement.getFraisoperateur()).isEqualTo(DEFAULT_FRAISOPERATEUR);
        assertThat(testPaiement.getTotalfrais()).isEqualTo(DEFAULT_TOTALFRAIS);
        assertThat(testPaiement.getConfirmepar()).isEqualTo(DEFAULT_CONFIRMEPAR);
        assertThat(testPaiement.getDatepaiement()).isEqualTo(DEFAULT_DATEPAIEMENT);
        assertThat(testPaiement.getDevise()).isEqualTo(DEFAULT_DEVISE);
        assertThat(testPaiement.getDateconfirmation()).isEqualTo(DEFAULT_DATECONFIRMATION);
    }

    @Test
    @Transactional
    public void createPaiementWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = paiementRepository.findAll().size();

        // Create the Paiement with an existing ID
        paiement.setId(1L);
        PaiementDTO paiementDTO = paiementMapper.toDto(paiement);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPaiementMockMvc.perform(post("/api/paiements")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paiementDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Paiement in the database
        List<Paiement> paiementList = paiementRepository.findAll();
        assertThat(paiementList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPaiements() throws Exception {
        // Initialize the database
        paiementRepository.saveAndFlush(paiement);

        // Get all the paiementList
        restPaiementMockMvc.perform(get("/api/paiements?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(paiement.getId().intValue())))
            .andExpect(jsonPath("$.[*].etat").value(hasItem(DEFAULT_ETAT.toString())))
            .andExpect(jsonPath("$.[*].methode").value(hasItem(DEFAULT_METHODE.toString())))
            .andExpect(jsonPath("$.[*].montantarticle").value(hasItem(DEFAULT_MONTANTARTICLE.doubleValue())))
            .andExpect(jsonPath("$.[*].fraisoperateur").value(hasItem(DEFAULT_FRAISOPERATEUR.doubleValue())))
            .andExpect(jsonPath("$.[*].totalfrais").value(hasItem(DEFAULT_TOTALFRAIS.doubleValue())))
            .andExpect(jsonPath("$.[*].confirmepar").value(hasItem(DEFAULT_CONFIRMEPAR.toString())))
            .andExpect(jsonPath("$.[*].datepaiement").value(hasItem(DEFAULT_DATEPAIEMENT.toString())))
            .andExpect(jsonPath("$.[*].devise").value(hasItem(DEFAULT_DEVISE.toString())))
            .andExpect(jsonPath("$.[*].dateconfirmation").value(hasItem(DEFAULT_DATECONFIRMATION.toString())));
    }

    @Test
    @Transactional
    public void getPaiement() throws Exception {
        // Initialize the database
        paiementRepository.saveAndFlush(paiement);

        // Get the paiement
        restPaiementMockMvc.perform(get("/api/paiements/{id}", paiement.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(paiement.getId().intValue()))
            .andExpect(jsonPath("$.etat").value(DEFAULT_ETAT.toString()))
            .andExpect(jsonPath("$.methode").value(DEFAULT_METHODE.toString()))
            .andExpect(jsonPath("$.montantarticle").value(DEFAULT_MONTANTARTICLE.doubleValue()))
            .andExpect(jsonPath("$.fraisoperateur").value(DEFAULT_FRAISOPERATEUR.doubleValue()))
            .andExpect(jsonPath("$.totalfrais").value(DEFAULT_TOTALFRAIS.doubleValue()))
            .andExpect(jsonPath("$.confirmepar").value(DEFAULT_CONFIRMEPAR.toString()))
            .andExpect(jsonPath("$.datepaiement").value(DEFAULT_DATEPAIEMENT.toString()))
            .andExpect(jsonPath("$.devise").value(DEFAULT_DEVISE.toString()))
            .andExpect(jsonPath("$.dateconfirmation").value(DEFAULT_DATECONFIRMATION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPaiement() throws Exception {
        // Get the paiement
        restPaiementMockMvc.perform(get("/api/paiements/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePaiement() throws Exception {
        // Initialize the database
        paiementRepository.saveAndFlush(paiement);
        int databaseSizeBeforeUpdate = paiementRepository.findAll().size();

        // Update the paiement
        Paiement updatedPaiement = paiementRepository.findOne(paiement.getId());
        // Disconnect from session so that the updates on updatedPaiement are not directly saved in db
        em.detach(updatedPaiement);
        updatedPaiement
            .etat(UPDATED_ETAT)
            .methode(UPDATED_METHODE)
            .montantarticle(UPDATED_MONTANTARTICLE)
            .fraisoperateur(UPDATED_FRAISOPERATEUR)
            .totalfrais(UPDATED_TOTALFRAIS)
            .confirmepar(UPDATED_CONFIRMEPAR)
            .datepaiement(UPDATED_DATEPAIEMENT)
            .devise(UPDATED_DEVISE)
            .dateconfirmation(UPDATED_DATECONFIRMATION);
        PaiementDTO paiementDTO = paiementMapper.toDto(updatedPaiement);

        restPaiementMockMvc.perform(put("/api/paiements")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paiementDTO)))
            .andExpect(status().isOk());

        // Validate the Paiement in the database
        List<Paiement> paiementList = paiementRepository.findAll();
        assertThat(paiementList).hasSize(databaseSizeBeforeUpdate);
        Paiement testPaiement = paiementList.get(paiementList.size() - 1);
        assertThat(testPaiement.getEtat()).isEqualTo(UPDATED_ETAT);
        assertThat(testPaiement.getMethode()).isEqualTo(UPDATED_METHODE);
        assertThat(testPaiement.getMontantarticle()).isEqualTo(UPDATED_MONTANTARTICLE);
        assertThat(testPaiement.getFraisoperateur()).isEqualTo(UPDATED_FRAISOPERATEUR);
        assertThat(testPaiement.getTotalfrais()).isEqualTo(UPDATED_TOTALFRAIS);
        assertThat(testPaiement.getConfirmepar()).isEqualTo(UPDATED_CONFIRMEPAR);
        assertThat(testPaiement.getDatepaiement()).isEqualTo(UPDATED_DATEPAIEMENT);
        assertThat(testPaiement.getDevise()).isEqualTo(UPDATED_DEVISE);
        assertThat(testPaiement.getDateconfirmation()).isEqualTo(UPDATED_DATECONFIRMATION);
    }

    @Test
    @Transactional
    public void updateNonExistingPaiement() throws Exception {
        int databaseSizeBeforeUpdate = paiementRepository.findAll().size();

        // Create the Paiement
        PaiementDTO paiementDTO = paiementMapper.toDto(paiement);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPaiementMockMvc.perform(put("/api/paiements")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paiementDTO)))
            .andExpect(status().isCreated());

        // Validate the Paiement in the database
        List<Paiement> paiementList = paiementRepository.findAll();
        assertThat(paiementList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePaiement() throws Exception {
        // Initialize the database
        paiementRepository.saveAndFlush(paiement);
        int databaseSizeBeforeDelete = paiementRepository.findAll().size();

        // Get the paiement
        restPaiementMockMvc.perform(delete("/api/paiements/{id}", paiement.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Paiement> paiementList = paiementRepository.findAll();
        assertThat(paiementList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Paiement.class);
        Paiement paiement1 = new Paiement();
        paiement1.setId(1L);
        Paiement paiement2 = new Paiement();
        paiement2.setId(paiement1.getId());
        assertThat(paiement1).isEqualTo(paiement2);
        paiement2.setId(2L);
        assertThat(paiement1).isNotEqualTo(paiement2);
        paiement1.setId(null);
        assertThat(paiement1).isNotEqualTo(paiement2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PaiementDTO.class);
        PaiementDTO paiementDTO1 = new PaiementDTO();
        paiementDTO1.setId(1L);
        PaiementDTO paiementDTO2 = new PaiementDTO();
        assertThat(paiementDTO1).isNotEqualTo(paiementDTO2);
        paiementDTO2.setId(paiementDTO1.getId());
        assertThat(paiementDTO1).isEqualTo(paiementDTO2);
        paiementDTO2.setId(2L);
        assertThat(paiementDTO1).isNotEqualTo(paiementDTO2);
        paiementDTO1.setId(null);
        assertThat(paiementDTO1).isNotEqualTo(paiementDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(paiementMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(paiementMapper.fromId(null)).isNull();
    }
}
